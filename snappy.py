import os
import sys

import sassylogic.main
import sassylogic.utils as utils

import nlg.nlg


def main():
    try:
        print('Setting up logging...')
        utils.try_setup_logging()
        utils.get_log().debug('logging set up')
    except Exception as e:
        print('Exception while setting up logging:' + str(e))
    try:
        nlg.nlg.init_from_settings(
            utils.find_data_file('resources', 'application.settings'))
    except Exception as e:
        utils.get_log().exception(e)
        try:
            nlg.nlg.init_from_settings(
                'sassynlg/nlg/resources/simplenlg.settings')
        except Exception:
            pass
    res = 0
    try:
        res = sassylogic.main.run_gui()
    except Exception as e:
        utils.get_log().exception(e)
    try:
        nlg.nlg.shutdown()
    except Exception as e:
        utils.get_log().exception(e)
    sys.exit(res)


if __name__ == "__main__":
    main()