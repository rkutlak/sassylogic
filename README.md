*SNAPPY*

---

Snappy is a program for translating First Order Predicate Logic formulas to natural language (English). It is implemented in Python 3 and uses sassynlg library for the language generation.

The program can use several simplification methods to reduce the input formula to a logically equivalent formula that can be expressed better in English. For example, double negations can be removed, vacuous quantifiers can be removed or Quinn-McKluskey algorithm can be used to simplify the formula (Karnaguh map simplification).

The program can also use theorem prover (Prover9) to simplify given formula using background information. This can work well in domains where many things are explicit but natural language does need to do so. For example, the description of an action `move` in PDDL might mention that object `a` must be at place `p` and after performing the action it is no longer at `a`. This level of explicitness is detrimental to NL presentation of the action.


