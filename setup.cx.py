"""
This file should allow building an executable on all platforms using cx_Freeze.
It is not working at the moment -- some of the Qt libraries are linked
to the locally installed Qt and I don't see how to fix them. As a result,
the executable is trying to load two versions of Qt libraries and crashes.

TODO: Test with a newer version of cx_Freeze

"""

from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need
# fine tuning.
includefiles = ['sassylogic/resources/']

options = {
    'build_exe': {
        'includes': ['os', 'atexit', 'numbers', 'pyparsing', 'rdflib'],
        'packages': ['sassylogic', 'nlg'],
        'excludes': ['tkinter'],
        'include_files': includefiles
    }
}

import sys

base = 'Console'
if sys.platform=='win32':
    base = 'Win32GUI'


executables = [
    Executable('snappy.py', base=base)
]


#long_description = read('README.txt', 'CHANGES.txt')
long_description = """
SNAPPY is a tool for simplifying propositional logic formulas 
and some First Order Predicate formulas. It can also express
the formulas in natural language.
"""



setup(name='Snappy',
      version = '0.1.0',
      url='http://bitbucket.org/rkutlak/sassylogic',
      license='BSD',
      description = 'SNAPPY is a tool for learning logic.',
      options = options,
      executables = executables,
      author='Roman Kutlak',
      author_email='roman@kutlak.net',
      long_description=long_description,
      platforms='any',
      classifiers = [
        'Programming Language :: Python',
        'Development Status :: 1 - Alpha',
        'Natural Language :: English',
#        'Environment :: Web Environment',
#        'Intended Audience :: Developers',
#        'License :: OSI Approved :: Apache Software License',
#        'Operating System :: OS Independent',
#        'Topic :: Software Development :: Libraries :: Python Modules',
#        'Topic :: Software Development :: Libraries :: Application Frameworks',
#        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        ]
)
