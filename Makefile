
USER=roman
SHELL=/bin/bash
# Note that everyone will have a different path.
#  Regardless of that, it should contain path to the python command and PyQt.
#export PATH := /Library/Frameworks/Python.framework/Versions/3.3/bin:$(PATH)

PYTHON=python3

# name of the package
package = sassylogic
app     = SNAPPY.app

OPTIONS =  #--resources resources/log.config.yaml,resources/images/yes.png,resources/images/no.png,prover9,resources/simplenlg.jar,resources/application.settings,heuristic.txt

# default target - run the app from place
debug: structure
	cd tmp && \
	$(PYTHON) setup.py py2app -A $(OPTIONS)
	cp -R $(package)/resources tmp/dist/$(app)/Contents/MacOS/

release: structure
	cd tmp && \
	$(PYTHON) setup.py py2app $(OPTIONS) && \
	cd dist/$(app)/Contents/ && \
	cp -R /Developer/Qt/5.3/clang_64/plugins/ PlugIns
	macdeployqt tmp/dist/$(app)
	cp -R $(package)/resources tmp/dist/$(app)/Contents/MacOS/

# autogenerate basic setup file -- prefer the one in main package
setup: structure
	cd tmp && \
	py2applet --make-setup $(package)/main.py

# create a new copy of the package
structure: clean
	mkdir tmp
	cp -R $(package) tmp/$(package)
	cp snappy.py tmp/
	cp -R sassynlg/nlg tmp/
	cp sassynlg/nlg/resources/simplenlg.jar tmp/$(package)/resources/
	cp setup.py tmp/

run:
	open tmp/dist/$(app)

all: release

.PHONY : clean
clean:
	rm -rf tmp


freeze:
	rm -rf tmp
	mkdir tmp
	cp -R $(package) tmp/$(package)
	cp snappy.py tmp
	cp -R sassynlg/nlg tmp/
	cp sassynlg/nlg/resources/simplenlg.jar tmp/$(package)/resources/
	cp setup.cx.py tmp/setup.py
	cd tmp && $(PYTHON) setup.py bdist_mac
	cp -R $(package)/resources tmp/dist/$(app)/Contents/MacOS/


################################################################################

# this target was supposed to check the syntax but it does not seem to work
build:
	pyflakes $(source_files)


print_source_files:
	echo $(source_files)

print_bytecode_files:
	echo $(bytecode_files)


# remove precompiled files
#.PHONY : clean
#clean :
#	-rm -rf src/__pycache__
#	-rm -f $(bytecode_files)


# Xcode and make docs: 
# https://developer.apple.com/library/mac/#documentation/DeveloperTools/Reference/XcodeBuildSettingRef/1-Build_Setting_Reference/build_setting_ref.html#//apple_ref/doc/uid/TP40003931-CH3-SW45
