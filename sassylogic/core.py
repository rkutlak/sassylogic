import os
import logging
from copy import deepcopy

from nlg.structures import (Element, String, Word, Clause, Phrase, Coordination,
                            NounPhrase, VerbPhrase, PrepositionalPhrase,
                            AdjectivePhrase, AdverbPhrase, PlaceHolder)

logging.getLogger(__name__).addHandler(logging.NullHandler())

def get_log():
    return logging.getLogger(__name__)


class Document:
    """ Main class for storing information such as predicate mappings. """

    def __init__(self, fileName=''):
        """ Create a new document, possibly by specifying the file path. 
        No loading is done in the constructor.
        
        """
        self._file_path = fileName
        self._modified = False
        self._templates = {}

    def __str__(self):
        """ Return a string representation of the document. """
        return 'Document: ' + str(self._templates)

    @property
    def templates(self):
        """ Return an iterator for the stored templates. """
        for x in self._templates.items():
            yield x

    @property
    def entries(self):
        """ Return an iterator over the keys and correspondig templates. """
        return self._templates.items()
    
    @property
    def keys(self):
        """ Return an iterator over the keys of the templates. """
        return self._templates.keys()

    def is_modified(self):
        """ Return True of doc was modified and should be saved. """
        return self._modified

    def file_name(self):
        """ Return the name of the current file. """
        return os.path.split(self._file_path)[1]

    def file_dir(self):
        """ Return the directory of the current file. """
        return os.path.split(self._file_path)[0]

    def file_path(self):
        """ Return the full path of the current file. """
        return self._file_path

    def set_file_path(self, fileName=''):
        """ Set a new filename (including path). Mark doc as not modified. """
        self._file_path = os.path.expanduser(fileName)

    def load(self, path):
        """ Load a document content (JSON) from a file given in path. """
        if not path: return False
        with open(path) as f:
#            self._templates = json.load(f, object_hook=ElemntCoder.from_json)
            self._templates = eval(f.read())
            self.set_file_path(path)
            self._modified = False
            return True

    def save(self):
        """ Save the document (JSON) into a file provided by the path. 
        When no path is provided, the file is saved in './untitled.logic'
        
        """
        if not self._file_path: self._file_path = './untitled.logic'
        with open(self._file_path, 'w') as f:
            # repr(x) contains too many white spaces
            #   -- remove them using split() and then ' '.join()
            rs = [(repr(k), ' '.join(repr(v).split()))
                    for k, v in self._templates.items()]
            f.write('{')
            for k, v in rs:
                f.write('{0}: {1},\n '.format(k, v))
            f.write('}')
            self._modified = False
            return True

    def add_template(self, k, v, override=False):
        """ Add a template v for predicate k. 
        Do not add multiple times - return False.
        
        """
        if k in self._templates and not override:
            return None
        self._templates[k] = v
        self._modified = True
        data = (k, v)
        return data

    def has_template(self, k):
        """ Return True if there is a template for the given key. """
        return (k in self._templates)

    def get_template(self, k):
        """ Return the template for the given key or raise a KeyError. """
        return self._templates[k]

    def copy_template(self, k):
        """ Return a copy of the template for given key or raise a KeyError. """
        return deepcopy(self._templates[k])

    def del_template(self, k):
        """ Delete a template corresponding to the key or raise a KeyError. """
        v = self._templates[k]
        del self._templates[k]
        self._modified = True
        return (k, v)

    def num_templates(self):
        """ Return the number of templates. """
        return len(self._templates)



