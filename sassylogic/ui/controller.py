import logging

from sassylogic.signals import Signal
from sassylogic.core import Document
import sassylogic.utils as utils


def get_log():
    return logging.getLogger(__name__)

get_log().addHandler(logging.NullHandler())

import nlg
import nlg.fol as fol
import nlg.lexicalisation as lex
from nlg.structures import Element
from nlg.macroplanning import formula_to_rst
import nlg.prover as prover
from nlg.nlg import Nlg
from nlg.fol import expr
import nlg.simplifications as simplifications

# comment: example precondition from DepotsNumeric.pddl
#exists hoist, crate, truck, place: 
#    At(hoist, place) & At(truck, place) & Lifting(hoist, crate) &
#    (current_load(truck) + weight(crate) <= load_limit(truck))


###############################  Interactivity  ################################


class Question:
    """A container class for passing an answer to a question."""
    def __init__(self, text='', options=None, default_opt=None):
        """ Create a new instance with no valid anser.
        The user is expected to set the "answer" to something (e.g., 'yes').
        text - string representing the question ('Save file?')
        options - a list or tuple of possible options ('yes', 'no', etc)
        default_opt - one of the default options
        
        """
        self.text = text
        self.options = options
        self.default_opt = default_opt
        self.answer = None


########################  Natural Language Generation  #########################
# P ∨ Q ∨ P

class NlgController:
    """ This class controlls the access to the NLG module. """
    
    ontology = None
    
    def __init__(self, interactive=True):
        self.nlg = Nlg()
        heur = utils.find_data_file('resources', 'heuristic.txt')
        self.simplification_ops = {
            'nnf': simplifications.nnf,
            'pnf': simplifications.pnf,
            'deepen': fol.deepen,
            'flatten': fol.flatten,
            'kleene': simplifications.kleene,
            'push_neg': simplifications.push_neg,
            'miniscope': simplifications.miniscope,
            'drop_quant': simplifications.drop_quant,
            'pull_quants': simplifications.pull_quants,
            'push_quants': simplifications.push_quants,
            'remove_conditionals': simplifications.remove_conditionals,
            'use_axioms': self.use_axioms,
            'search': lambda x: simplifications.minimise_search(x, heur),
            'QM': simplifications.minimise_qm,
        }

    def parse_constants(self, text):
        pass

    def translate_formula(self, f, simple, ontology=None, context=None):
        """ Take a formula and return corresponding English text. """
        get_log().debug('translate_formula(%s)' % repr(f))
        # create an nlg document
        d = formula_to_rst(f)
        # realise the document
        if simple or not nlg.simplenlg_server:
            t = self.nlg.process_nlg_doc(d, ontology, context)
        else:
            t = self.nlg.process_nlg_doc2(d, ontology, context)
        get_log().debug('...translated as "%s"' % t)
        return t

    def translate_formulas(self, fs, simple):
        """ Take a list of formulas and return corresponding English text. """
        get_log().debug('translate_formulas(%s)' % repr(fs))
        onto = None
        ctx = nlg.reg.Context()
        res = [self.translate_formula(f, simple, onto, ctx) for f in fs]
        t = ' '.join(res)
        return t
    
    def parse_formula(self, text):
        """ Parse given text and return the corresponding Expr.
        Raise FormulaParseError if the text does not parse.
        
        """
        return expr(text)
    
    def parse_formulas(self, text, sep=';'):
        """ Parse a list of formulas separated by sep. 
        Raise FormulaParseError if the text does not parse.
        
        """
        return [expr(x.strip()) for x in text.split(sep) if x.strip()]

    def simplify_formula(self, f, simpl_ops_str, axioms):
        """ Apply simplification operations to the given formula. """
        get_log().debug('Simplifying formula using ops "{0}'
                        .format(simpl_ops_str))
        ops = reversed(simpl_ops_str.split('.'))
        res = f
        for op in ops:
            if op in self.simplification_ops:
                if 'axioms' in op:
                    do_simpl = 'kleene' in simpl_ops_str
                    res = self.simplification_ops[op](res, axioms, do_simpl)
                else:
                    res = self.simplification_ops[op](res)
            else:
                get_log().error('Simplification method "{0}"'
                                ' not found -- ignored.'.format(op))
        return res

    def simplify_formulas(self, fs, simpl_ops_str, axioms):
        res = [self.simplify_formula(f, simpl_ops_str, axioms) for f in fs]
        return res

    def use_axioms(self, f, axioms, kleene=False):
        get_log().debug('Simplifying formula {0} using axioms.'.format(f))
        # first test for a tautology:
        is_tautology = prover.test_tautology(f, axioms)
        if is_tautology:
            get_log().debug('Formula "{0}" is a tautology.'.format(f))
            return expr('True')
        # then contradiction
        is_contradiction = prover.test_contradiction(f, axioms)
        if is_contradiction:
            get_log().debug('Formula "{0}" is a contradiction.'.format(f))
            return expr('False')
        # if the formula is not a tautology and not a contradiction,
        # see if we can simplify it
        formulas = list(fol.generate_subformulas(f))
        for c in formulas:
            get_log().debug('\t' + str(c))
        candidates = []
        for fm in formulas:
            if kleene:
                if prover.test_equivalent(f, simplifications.kleene(fm), axioms):
                    candidates.append(fm)
            else:
                if prover.test_equivalent(f, fm, axioms):
                    candidates.append(fm)
        if len(candidates) == 0:
            get_log().debug('No simplified candidates found.')
            return f
        # now sort by number of operators and pick the shortest exp?
        get_log().debug('Simplified candidates:'.format(f))
        for c in sorted(candidates, key=lambda x: x.numops()):
            get_log().debug('\t' + str(c))
        return min(candidates, key=lambda x: x.numops())

    def on_file_loaded(self, doc):
        """ Read the templates in the Document 
        and add them into the lexicaliser.
        
        """
        get_log().debug('loading templates from document "%s"' %
                        doc.file_name())
        if not doc: return
        for k, v in doc.templates:
            lex.add_template(k, v)
            
    def on_file_closed(self, doc):
        """ Delete the templates in the Document. """
        get_log().debug('removing templates from document "%s"' %
                doc.file_name())
        if not doc: return
        for k, v in doc.templates:
            lex.del_template(k)
            
    def on_template_added(self, template):
        """ Add a template to the lexicaliser. """
        get_log().debug('added template "{0}"'.format(template))
        if template: lex.add_template(*template)

    def on_template_deleted(self, template):
        """ Remove a template from the lexicaliser. """
        get_log().debug('deleted template "{0}"'.format(template))
        if template:
            k, v = template
            lex.del_template(k)


##############################  Theorem Proving  ###############################


from nltk.sem import logic
logic._counter._value = 0
from nltk import Model, Valuation, Assignment
from nltk import Prover9Command, MaceCommand


class LogicController:
    """ This class provides theorem-proving related functionality. """
    lp = logic.LogicParser()
    valuation = None
    
    def parse_valuation(self, text):
        try:
            self.valuation = Valuation.fromstring(text)
            return self.valuation
        except Exception as e:
            self.valuation = None
            get_log().exception(str(e))
            return str(e)

    def create_valuation(self, axioms_text):
        try:
            axioms = self.parse_axioms(axioms_text)
            get_log().debug('Axioms passed to Mace:\n{0}'.format(axioms))
            mc = MaceCommand(None, axioms)
            get_log().debug(mc)
            can_model = mc.build_model()
            if can_model:
                self.valuation = mc.valuation
                get_log().debug(self.valuation)
                return self.valuation
            else:
                self.valuation = None
                return ('Mace could not build a model '
                        'from the given background theory.')
        except Exception as e:
            self.valuation = None
            get_log().exception(str(e))
            return str(e)
    
    def create_counter_model(self, goal_text, axioms_text, max=100):
        try:
            axioms = self.parse_axioms(axioms_text)
            goal = self.parse_formula(goal_text)
            get_log().debug('Mace input:\n\tgoal: {0}\n\taxioms:{1}'.
                            format(goal, axioms))
            mc = MaceCommand(goal, axioms, max_models=max)
            can_model = mc.build_model()
            if can_model:
                self.valuation = mc.valuation
                get_log().debug(self.valuation)
                return self.valuation
            else:
                self.valuation = None
                return ('Mace could not build a counter model '
                        'from the given goal and background theory.')
        except Exception as e:
            self.valuation = None
            get_log().exception(str(e))
            return str(e)
    
    def create_model(self, axioms_text, max=100):
        try:
            axioms = self.parse_axioms(axioms_text)
            get_log().debug('Mace input:\n\tgoal: {0}\n\taxioms:{1}'.
                            format(None, axioms))
            mc = MaceCommand(None, axioms, max_models=max)
            can_model = mc.build_model()
            if can_model:
                self.valuation = mc.valuation
                get_log().debug(self.valuation)
                return self.valuation
            else:
                self.valuation = None
                return ('Mace could not build a counter model '
                        'from the given background theory.')
        except Exception as e:
            self.valuation = None
            get_log().exception(str(e))
            return str(e)

    def prove_theorem(self, g, axioms_text, max=20):
        try:
            axioms = self.parse_axioms(axioms_text)
            goal = self.parse_formula(g)
            get_log().debug('Prover input:\n\tgoal: {0}\n\taxioms:{1}'.
                            format(goal, axioms))
            pc = Prover9Command(goal, axioms, timeout=max)
            if pc.prove():
                return pc
            else:
                return ('Theorem prover could not prove the goal from '
                        'the given background axioms')
        except Exception as e:
            get_log().exception(str(e))
            return str(e)

    def evaluate_formula(self, fml, assign='[]', val='', dom=''):
        try:
            # if possible, use existing valuation; else use one from a string
            formula = self.parse_formula(fml)
            valuation = self.valuation or Valuation.fromstring(val)
            domain = eval(dom) if dom else valuation.domain
            m = Model(domain, valuation)
            e = eval(assign)
            get_log().debug('Evaluating formula with assignment: ' + repr(e))
            assignment = Assignment(domain, e)
            res = m.evaluate(str(formula), assignment)
            return ('The formula "{f}" is {res} in the following model:\n{m}'
                    .format(f=str(formula), res=res, m=str(m)))
        except Exception as e:
            get_log().exception(str(e))
            return str(e)

    def find_satisfiers(self, fml, var, assign='[]', val='', dom=''):
        try:
            # if possible, use existing valuation; else use one from a string
            formula = self.parse_formula(fml)
            valuation = self.valuation or Valuation.fromstring(val)
            domain = eval(dom) if dom else valuation.domain
            m = Model(domain, valuation)
            e = eval(assign)
            get_log().debug('Searching satisfiers with assignment: ' + repr(e))
            assignment = Assignment(domain, e)
            res = m.satisfiers(formula, var, assignment)
            return ('The formula "{f}" can be satisfied by "{res}" '
                    'in the model:\n{m}'.format(f=str(formula),
                                                res=res, m=str(m)))
        except Exception as e:
            get_log().exception(str(e))
            return str(e)

    def parse_axioms(self, text, sep=';', tryagain=True):
        try:
            # try to parse the formulas using NLTK syntax
            return [self.parse_formula(x) for x in text.split(sep) if x.strip()]
        except Exception as e:
            if tryagain:
                # if that does not work, try nlg.fol syntax
                tmp = [expr(x) for x in text.split(sep) if x.strip()]
                tmp_text = ';'.join([fol.to_prover_str(x) for x in tmp])
                res = self.parse_axioms(tmp_text, sep=sep, tryagain=False)
                return res
            else:
                get_log().exception(str(e))
                raise Exception from e

    def parse_formula(self, text, tryagain=True):
        try:
            # try to parse the formulas using NLTK syntax
            return self.lp.parse(text)
        except Exception as e:
            if tryagain:
                # if that does not work, try nlg.fol syntax
                tmp = expr(text)
                tmp_text = fol.to_prover_str(tmp)
                res = self.parse_formula(tmp_text, tryagain=False)
                return res
            else:
                get_log().exception(str(e))
                raise Exception from e


#################################  Document  ###################################


class DocumentController:
    """ This class controlls the access to a document. 
    It provides convenience functionality such as asking a user
    whether a document that was modified should be saved, etc.
    In order for interactivity to occur, the client has to implement
    the relevant slots responding to the following signals:
        - ask: ask whether to save a file, or any other question
        - ask_open_file_path: ask for the path of a document
                                to open if not provided
        - ask_save_file_path: ask for the path where to save 
                                the current document if not provided
    When an "ask" signal is raised, it passes a Question as a parameter.
                                
    Additional signals include:
        - template_added: notifies the client that a template was added,
            passing the template as a parameter
        - template_deleted: notifies the client that a template was deleted,
            passing the template as a parameter
        - file_created: a new file was created; returns the document
        - file_loaded: a new content was loaded; returns the document
        - file_saved: file was saved; 
            is_modified() will return True; returns the document 
        - file_closed: returns the document
        - file_path_changed: the path (or name) of the file changed; 
            returns the document

    """

    def __init__(self, interactive=True):
        """ Initialise the controller. Interactive=True by default. """
        self.doc = Document() # the current document
        # should the client be asked about modifications?
        self._interactive = interactive
        # interactivity signals
        self.ask = Signal()
        self.ask_open_file_path = Signal()
        self.ask_save_file_path = Signal()
        # signals for reporting changes
        self.template_added = Signal()
        self.template_deleted  = Signal()
        self.modified = Signal()
        self.file_created = Signal()
        self.file_loaded = Signal()
        self.file_saved = Signal()
        self.file_closed = Signal()
        self.file_path_changed = Signal()

    def _ask_open_file_path(self):
        """ Ask for path to open a Document and return the answer. """
        a = Question('Open file...', ('open', 'cancel'))
        self.ask_open_file_path(a) # trigger the signal
        return a
        
    def _ask_save_file_path(self):
        """ Ask for path to save the Document and return the answer. """
        a = Question('Save file as...', ('save', 'cancel'))
        self.ask_save_file_path(a)
        return a

    def _maybe_save_and_continue(self, document):
        """ If the current Document is modified and the controller 
        is interactive, the user is asked whether to save the Document or not.
        The function returns True if the caller should proceed (either because
        the user agreed to save the document and saving was successful or
        because the controller is not interactive). The method returns False
        if the user cancelled the operation.
        
        """
        if not document or not document.is_modified():
            return True
        
        if self._interactive:
            # TODO: change to 'save', 'discard'
            msg = ("The document has been modified.\n"
                   "Do you want to save your changes?")
            vals = ('yes', 'no', 'cancel')
            default_opt = 'yes'
            
            q = Question(msg, vals, default_opt)
            self.ask(q)
            if q.answer is None: return True # no answer provided --> proceed
            if q.answer == 'yes': return self.save()
            if q.answer == 'no':  return True
            if q.answer == 'cancel': return False
        # non-interactive --> proceed
        return True
        
    def new(self):
        """ Create a new Document by closing (and optionally saving) 
        the current Document. Notify any listeners that a Document was created.
        See close() for details.
        
        """
        get_log().debug('new')
        if self.close():
            self.file_created(self.doc)
        return False

    def open(self, path, interactive=None):
        """ Close the current document and load a new one. If the controller 
        is interactive and the path of the document to open is None, 
        the controller will ask the user for a path. The signal file_loaded
        is emited on success.
        
        """
        get_log().debug('open')
        if interactive is None: interactive = self._interactive
        if self.close():
            if not path and interactive:
                a = self._ask_open_file_path()
                return self.open(a.answer, interactive=False)
            else:
                res = self.doc.load(path)
                self.file_loaded(self.doc)
                return res
        return False

    def save(self):
        """ Save the current Document. If the document does not have a name,
        call save_as. The signal file_saved is emited on success.
        
        """
        get_log().debug('save')
        if not self.doc.file_name():
            return self.save_as(None)
        else:
            res = self.doc.save()
            self.file_saved(self.doc)
            return res

    def save_as(self, path, interactive=None):
        """ Save the current Document as a different Document 
        and the emit file_saved signal. When path is None and the controller
        is interactive, ask the user for a path. The signal file_saved
        is emited on success.
        
        """
        get_log().debug('save as')
        if interactive is None: interactive = self._interactive
        # if no path was provided, ask and try again
        if not path and interactive:
            a = self._ask_save_file_path()
            return self.save_as(a.answer, interactive=False)
        self.doc.set_file_path(path)
        self.doc.save()       
        self.file_saved(self.doc)
        return True

    def close(self):
        """ Close the current Document and create a new one. Notify listeners
        of file_closed signal. If the controller is interactive, a user 
        might be asked whether to save changes.
        
        """
        get_log().debug('closing doc; modified=' + str(self.doc.is_modified()))
        if self._maybe_save_and_continue(self.doc):
            self.file_closed(self.doc)
            self.doc = Document()
            return True
        return False

    def set_file_path(self, fileName=''):
        """ Set a new filename (including path) and emit the change signal. """
        self.doc.set_file_path(fileName)
        self.file_path_changed(self.doc)
        return True

    def add_template(self, k, v, override=False):
        """ Add a template v for predicate k.
        Raise signals item_added and modified and return the (k, v) pair.
        
        """
        get_log().debug('Adding the template {0}:\n{1}'.format(k, v))
        value = None
        if isinstance(v, str):
            value = lex.create_template_from(v)
            if not isinstance(value, Element):
                raise Exception('The string is not in a correct format:\n' +
                                repr(value))
        elif isinstance(v, Element):
            value = v
        else:
            raise Exception('Trying to add a template '
                            'that is not a str or Element: %s' % type(v))
                            
        get_log().error('Value is '.format(repr(value)))
        old_val = ''
        if self.doc.has_template(k):
            old_val = self.doc.get_template(k)
        get_log().debug('Old template {0}'.format(old_val))
        res = self.doc.add_template(k, value, override)
        get_log().debug('Result of adding template {0}'.format(res))
        if res:
            if old_val != '':
                self.template_deleted( (k, old_val) )
            self.template_added(res)
            self.modified(res)
        return res

    def get_template(self, k):
        """ Return a copy of a template for given key or raise an error. """
        return self.doc.copy_template(k)

    def del_template(self, k):
        """ Delete a template corresponding to the key k if it exists. 
        Otherwise, do nothing. If an item was deleted, return it.
        
        """
        res = self.doc.del_template(k)
        if res:
            self.template_deleted(res)
            self.modified(res)
        return res

# stackoverflow.com/questions/1395913/python-drop-into-repl-read-eval-print-loop
#def interact():
#	import code
#	code.InteractiveConsole(locals=globals()).interact()

#############################################################################
##
## Copyright (C) 2014 Roman Kutlak, University of Aberdeen.
## All rights reserved.
##
## This file is part of SAsSy Logic program.
##
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of University of Aberdeen nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################