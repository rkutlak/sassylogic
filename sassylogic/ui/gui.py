import os
import sys
import logging


def get_log():
    return logging.getLogger(__name__)

get_log().addHandler(logging.NullHandler())


import PyQt5.QtGui as QtGui
import PyQt5.QtCore as QtCore
import PyQt5.QtWidgets as QtWidgets

from PyQt5.QtCore import Qt, QAbstractListModel
from PyQt5.QtCore import QModelIndex, QVariant
from PyQt5.QtGui import QFontMetrics

from sassylogic.ui.mainwindow import Ui_SassyLogic
from sassylogic.ui.helpdialog import Ui_HelpDialog
from sassylogic.ui.snappymainwindow import Ui_SNAPPY
from sassylogic.ui.controller import DocumentController, NlgController
from sassylogic.ui.controller import Question, LogicController
from sassylogic.ui.highlighter import FOLHighlighter, TemplateHighlighter
from sassylogic.ui.editor import LogicCompleter
from nlg.fol import FormulaParseError


class HelpDialog(QtWidgets.QDialog, Ui_HelpDialog):
    def __init__(self, parent=None, f=QtCore.Qt.WindowFlags(), text=''):
        super(QtWidgets.QDialog, self).__init__(parent, f)
        self.setupUi(self)

    def closeEvent(self, e):
        self.hide()
        e.accept()


class SassyLogicMainWindow(QtWidgets.QMainWindow, Ui_SassyLogic):
    def __init__(self, parent=None, f=QtCore.Qt.WindowFlags()):
        QtWidgets.QMainWindow.__init__(self, parent, f)

        self.setupUi(self)
        # file menu
        self.actionNew.triggered.connect(self.handle_new)
        self.actionOpen.triggered.connect(self.handle_open)
#        self.actionClear.triggered.connect(self.handle_clear_recent)
        self.actionSave.triggered.connect(self.handle_save)
        self.actionSave_As.triggered.connect(self.handle_save_as)
        self.actionPrint.triggered.connect(self.handle_print)
        self.actionClose.triggered.connect(self.handle_close)
        # buttons
        #    NLG Templates
        self.addMappingButton.clicked.connect(self.handle_add_mapping)
        self.updateMappingButton.clicked.connect(self.handle_update_mapping)
        self.deleteMappingButton.clicked.connect(self.handle_delete_mapping)
        self.addMappingButton.setEnabled(False)
        self.deleteMappingButton.setEnabled(False)
        self.updateMappingButton.setEnabled(False)
        #    NLG
        self.translateButton.clicked.connect(self.handle_translate)
        #    Model
        self.parseValuationButton.clicked.connect(self.handle_parse_valuation)
        self.createValuationButton.clicked.connect(self.handle_create_valuation)
        #    Theorem Proving
        self.createCounterModelButton.clicked\
            .connect(self.handle_create_counter_model)
        self.createModelButton.clicked.connect(self.handle_create_model)
        self.proveTheoremButton.clicked.connect(self.handle_prove_theorem)
        self.evaluateFormulaButton.clicked.connect(self.handle_evaluate_formula)
        self.findSatisfiersButton.clicked.connect(self.handle_find_satisfiers)
        self.clearProvingOutputButton.clicked\
            .connect(self.handle_clear_proving_output)
        self.simplificationHelpToolButton.clicked.connect(self.show_help)
        # navigation shortcuts
        self.actionTheorem_Proving.triggered.connect(self.show_proving)
        self.actionBackground_Theory.triggered.connect(self.show_bgt)
        self.actionModel.triggered.connect(self.show_model)
        self.actionNLG.triggered.connect(self.show_nlg)
        self.actionNLG_Templates.triggered.connect(self.show_templates)
        self.actionNLG_Domain.triggered.connect(self.show_domain)
        # signals
        self.predicateText.textChanged.connect(self.on_predicate_text_changed)
        self.templateText.textChanged.connect(self.on_template_text_changed)
        # Logic controller
        self.logic_ctrl = LogicController()
        # Document controller
        self.doc_ctrl = DocumentController()
        self.doc_ctrl.ask_save_file_path.connect(self.get_save_file_path)
        self.doc_ctrl.ask_open_file_path.connect(self.get_open_file_path)
        self.doc_ctrl.ask.connect(self.ask_question)
        self.doc_ctrl.file_loaded.connect(self.on_file_loaded)
        self.doc_ctrl.file_saved.connect(self.on_file_saved)
        self.doc_ctrl.file_closed.connect(self.on_file_closed)
        self.doc_ctrl.template_added.connect(self.on_template_added)
        self.doc_ctrl.template_deleted.connect(self.on_template_deleted)
        self.doc_ctrl.modified.connect(self.on_file_modified)
        # initialise the default model
        self.on_file_closed(None)
        # NLG controller
        self.nlg_ctrl = NlgController()
        # link it with the doc controller so that templates can be loaded
        self.doc_ctrl.template_deleted.connect(
            self.nlg_ctrl.on_template_deleted)
        self.doc_ctrl.template_added.connect(
            self.nlg_ctrl.on_template_added)
        self.doc_ctrl.file_closed.connect(
            self.nlg_ctrl.on_file_closed)
        self.doc_ctrl.file_loaded.connect(
            self.nlg_ctrl.on_file_loaded)
        self.inputText.setFocus()
        # highlighting for FOL
        FOLHighlighter(self.inputText.document())
        self.inputText.setCompleter(LogicCompleter())
        FOLHighlighter(self.provingInputText.document())
        self.provingInputText.setCompleter(LogicCompleter())
        FOLHighlighter(self.backgroundTheoryText.document())
        self.backgroundTheoryText.setCompleter(LogicCompleter())
        # highlighting for NLG templates
        keywords = ['Element', 'String', 'PlaceHolder', 'Word',
            'Noun', 'Verb', 'Adjective', 'Adverb', 'Preposition', 'Determiner',
            'Pronoun', 'Conjunction', 'Preposition', 'Numeral', 'Symbol',
            'Clause', 'NP', 'VP', 'PP', 'AdvP', 'AdjP',
            'NN', 'NNP', 'NounPhrase', 'VerbPhrase',
            'AdjectivePhrase', 'AdverbPhrase', 'PrepositionalPhrase',
            'Coordination',
            'Features', 'Case', 'Number', 'Gender', 'Person', 'Tense',
            'Aspect', 'Mood', 'Modal', 'Voice', 'Form', 'InterrogativeType'
        ]
        TemplateHighlighter(self.templateText.document(), keywords)
        self.templateText.add_delimiters('(', ')')
        self.templateText.setCompleter(QtWidgets.QCompleter(keywords,
                                           self.templateText))
        self.helpButton.clicked.connect(self.show_help)
        self.splitter_2.setSizes((200, 0))
        self.parserResponseLabel.setText('')

    def show_help(self):
        """ Create a new label and write the syntax on it. """
        d = HelpDialog(text=syntax_def)
        d.show()
        d.exec_()
    
    def show_proving(self):
        get_log().debug('showing theorem proving tab')
        self.tabWidget.setCurrentIndex(0)
    
    def show_bgt(self):
        get_log().debug('showing background theory tab')
        self.tabWidget.setCurrentIndex(1)
        
    def show_model(self):
        get_log().debug('showing model tab')
        self.tabWidget.setCurrentIndex(2)
        
    def show_nlg(self):
        get_log().debug('showing NLG tab')
        self.tabWidget.setCurrentIndex(3)
    
    def show_templates(self):
        get_log().debug('showing NLG templates tab')
        self.tabWidget.setCurrentIndex(4)
        
    def show_domain(self):
        get_log().debug('showing NLG domain tab')
        self.tabWidget.setCurrentIndex(5)

    def handle_new(self):
        get_log().debug('new')
        self.doc_ctrl.new()

    def handle_open(self):
        get_log().debug('open')
        return self.doc_ctrl.open(None) # ask for path
        
    def get_open_file_path(self, question):
        fn = QtWidgets.QFileDialog.getOpenFileName(self, "Open File...", None,
                "SassyLogic-Files (*.logic);;All Files (*)")
        if fn and fn[0] != '':
            question.answer = fn[0]
            return True
        return False

    def handle_save(self):
        get_log().debug('save')
        return self.doc_ctrl.save()
        
    def get_save_file_path(self, question):
        fn = QtWidgets.QFileDialog.getSaveFileName(self, "Save as...", None,
            "Sassy Logic files (*.logic);;Text-Files (*.txt);;All Files (*)")

        if not fn or fn[0] == '':
            return False

        question.answer = fn[0]
        return True

    def handle_save_as(self):
        get_log().debug('save as')
        q = Question()
        if self.get_save_file_path(q):
            return self.doc_ctrl.save_as(q.answer)
        return False

    def handle_print(self):
        get_log().debug('print...not implemented')

    def handle_close(self, e):
        get_log().debug('close')
        return self.doc_ctrl.close()
            
    def closeEvent(self, e):
        get_log().debug('close')
        if self.doc_ctrl.close():
            e.accept()
        else:
            e.ignore()


# handles for Model
    def handle_parse_valuation(self):
        text = self.valuationInputText.toPlainText()
        val = self.logic_ctrl.parse_valuation(text)
        if not isinstance(val, str):
            self.valuationOutputText.setText(str(val))
            self.domainText.setText(str(val.domain))
        else:
            self.valuationOutputText.setText('')

    def handle_create_valuation(self):
        axioms = self.backgroundTheoryText.toPlainText()
        val = self.logic_ctrl.create_valuation(axioms)
        if not isinstance(val, str):
            self.valuationOutputText.setText(str(val))
            self.domainText.setText(str(val.domain))
        else:
            self.valuationOutputText.setText('')

# handles for Theorem Proving
    def handle_create_counter_model(self):
        max = self.modelSizeBox.value()
        axioms = self.backgroundTheoryText.toPlainText()
        goal = self.provingInputText.toPlainText()
        val = self.logic_ctrl.create_counter_model(goal, axioms, max)
        if not isinstance(val, str):
            self.valuationOutputText.setText(str(val))
            self.domainText.setText(str(val.domain))
        else:
            self.valuationOutputText.setText('')
        self.provingOutputText.append(str(val))
        self.provingOutputText.ensureCursorVisible()

    def handle_create_model(self):
        max = self.modelSizeBox.value()
        axioms = self.backgroundTheoryText.toPlainText()
        val = self.logic_ctrl.create_model(axioms, max)
        if not isinstance(val, str):
            self.valuationOutputText.setText(str(val))
            self.domainText.setText(str(val.domain))
        else:
            self.valuationOutputText.setText('')
        self.provingOutputText.append(str(val))
        self.provingOutputText.ensureCursorVisible()

    def handle_prove_theorem(self):
        axioms = self.backgroundTheoryText.toPlainText()
        goal = self.provingInputText.toPlainText()
        max = self.proverTimeoutBox.value()
        proof = self.logic_ctrl.prove_theorem(goal, axioms, max)
        if not isinstance(proof, str):
            self.provingOutputText.append('The theorem is {res}\n'
                'Goal: {goal}\nAssumptions:\n\t{ass}'
                .format(res=proof.prove(),
                        goal=str(proof.goal()),
                        ass='\n\t'.join([str(a) for a in proof.assumptions()])))
            self.provingOutputText.append(str(proof.proof()))
        else:
            self.provingOutputText.append(proof)
        self.provingOutputText.ensureCursorVisible()

    def handle_evaluate_formula(self):
        goal = self.provingInputText.toPlainText().strip()
        assign = self.variableAssignmentText.toPlainText().strip()
        if not assign: assign = '[]'
        res = self.logic_ctrl.evaluate_formula(goal, assign)
        self.provingOutputText.append(str(res))
        if assign != '[]':
            self.provingOutputText.append('Variable assignment:\n' + assign)
        self.provingOutputText.ensureCursorVisible()

    def handle_find_satisfiers(self):
        var = self.satisfiersLine.text().strip()
        if not var:
            msg = 'Please provide the name of the variable.'
            self.provingOutputText.append(msg)
            return
        goal = self.provingInputText.toPlainText().strip()
        assign = self.variableAssignmentText.toPlainText().strip()
        if not assign: assign = '[]'
        res = self.logic_ctrl.find_satisfiers(goal, var, assign)
        if res == set(): res = '{}'
        self.provingOutputText.append(str(res))
        if assign != '[]':
            self.provingOutputText.append('Variable assignment:\n' + assign)
        self.provingOutputText.ensureCursorVisible()

    def handle_clear_proving_output(self):
        self.provingOutputText.setText('')

    def show_help(self):
        """Show the help dialog. """
        help = HelpDialog(parent=self)
        help.show()

# handles for edit menu

    def handle_add_mapping(self):
        if self.addMappingButton.text() == 'New Template':
            get_log().debug('new mapping')
            # a template was selected before - clear the selection
            self.addMappingButton.setText('Add')
            self.addMappingButton.setEnabled(False)
            self.predicateText.setText('')
            self.templateText.setText('')
            return
        # otherwise, add new mapping
        get_log().debug('add mapping')
        predicate = self.predicateText.toPlainText()
        template = self.templateText.toPlainText()
        try:
            self.doc_ctrl.add_template(predicate, template)
            self.predicateText.setText('')
            self.templateText.setText('')
        except Exception as e:
            get_log().exception('Exception while adding a template.')
            QtWidgets.QMessageBox.warning(self, "Application", str(e), 
                QtWidgets.QMessageBox.Ok)

    def handle_delete_mapping(self):
        """ User wants to delete mapping so remove it from the document. """
        get_log().debug('delete mapping')
        predicate = self.predicateText.toPlainText()
#        if not predicate: return False
        try:
            self.doc_ctrl.del_template(predicate)
            self.predicateText.setText('')
            self.templateText.setText('')
        except Exception as e:
            QtWidgets.QMessageBox.warning(self, "Application", str(e), 
                QtWidgets.QMessageBox.Ok)

    def handle_update_mapping(self):
        """ Update existing template. """
        get_log().debug('update mapping')
        predicate = self.predicateText.toPlainText()
        template = self.templateText.toPlainText()
        get_log().debug('Predicate "{0}" will map to:\n{1}'
                        .format(predicate, template))
        try:
            self.doc_ctrl.add_template(predicate, template, override=True)
            self.predicateText.setText('')
            self.templateText.setText('')
            self.updateMappingButton.setEnabled(False)
        except Exception as e:
            QtWidgets.QMessageBox.warning(self, "Application", str(e), 
                QtWidgets.QMessageBox.Ok)

                
# handle translation

    def handle_translate(self):
        get_log().debug('translate')
        text = self.inputText.toPlainText()
        try:
            f = self.nlg_ctrl.parse_formulas(text)
            t = self.nlg_ctrl.translate_formulas(f, simple=False)
            f_str = str(f)
            get_log().debug('Formula translated as:\n{0}'.format(f_str))
            self.parserResponseLabel.setText('Formula parsed correctly.')
            self.parserStatusLabel.setPixmap(QtGui.QPixmap(":/images/yes.png"))
        except FormulaParseError as e:
            self.parserResponseLabel.setText(str(e))
            self.parserStatusLabel.setPixmap(QtGui.QPixmap(":/images/no.png"))
            t = ''
            f = ''
            f_str = ''

        # show the text to the user
        self.directTranslationText.setPlainText(t)
        self.directFormulaText.setText(f_str)
        
        if not f: return
        # apply simplifications
        try:
            simpl_ops_str = self.simplificationMethodEdit.text()
            axioms = self.get_axioms()
            f2 = self.nlg_ctrl.simplify_formulas(f, simpl_ops_str, axioms)
            t2 = self.nlg_ctrl.translate_formulas(f2, simple=True)
            f2_str = str(f2)
            get_log().debug('Simplified formula translated as:\n{0}'
                            .format(f2_str))
        except Exception as e:
            get_log().exception('Exception while simplifying formula\n{0}'
                                .format(f_str))
            get_log().exception(str(e))
            try:
                f2 = self.nlg_ctrl.parse_formulas(text)
                t2 = self.nlg_ctrl.translate_formulas(f, simple=True)
                f2_str = str(f)
            except Exception:
                pass
        
        ## optimised translation
        self.simplifiedTranslationText.setPlainText(t2)
        self.simplifiedFormulaText.setText(f2_str)

    def get_axioms(self):
        """ Parse the set of axioms from the model tab and return them as 
        a list of Expr.
        
        """
        axioms = []
        try:
            text = self.backgroundTheoryText.toPlainText()
            axioms = self.nlg_ctrl.parse_formulas(text)
        except FormulaParseError as e:
            self.parserResponseLabel.setText('Error in parsing '
                'Background Theory:\n{0}'.format(e))
            self.parserStatusLabel.setPixmap(QtGui.QPixmap(":/images/no.png"))
        return axioms
    

# handle questions

    def ask_question(self, q):
        buttons = QtWidgets.QMessageBox.Cancel
        if 'save' in q.options:
            buttons |= QtWidgets.QMessageBox.Save
        if 'discard' in q.options:
            buttons |= QtWidgets.QMessageBox.Discard
        if 'yes' in q.options:
            buttons |= QtWidgets.QMessageBox.Yes
        if 'no' in q.options:
            buttons |= QtWidgets.QMessageBox.No
        
        mb = QtWidgets.QMessageBox
        ret = mb.warning(self, "Application", q.text, buttons)

        if ret == QtWidgets.QMessageBox.Save:
            q.answer = 'save'
        if ret == QtWidgets.QMessageBox.Discard:
            q.answer = 'discard'
        if ret == QtWidgets.QMessageBox.Yes:
            q.answer = 'yes'
        if ret == QtWidgets.QMessageBox.No:
            q.answer = 'no'
        if ret == QtWidgets.QMessageBox.Cancel:
            q.answer = 'cancel'

# respond to signals

    def on_file_loaded(self, path):
        shownName = self.doc_ctrl.doc.file_name()
        self.setWindowTitle(self.tr("%s[*] - %s" % (shownName, "Sassy Logic")))
        self.setWindowModified(False)
        strings = doc_to_string(self.doc_ctrl.doc)
        model = QtCore.QStringListModel(strings,
                                        parent=self.mappingsListView)
        model.sort(0)
        self.mappingsListView.setModel(model)
        sm = self.mappingsListView.selectionModel()
        sm.currentChanged.connect(self.on_template_selected)

    def on_file_saved(self, path):
        shownName = self.doc_ctrl.doc.file_name()
        self.setWindowTitle(self.tr("%s[*] - %s" % (shownName, "Sassy Logic")))
        self.setWindowModified(False)
        
    def on_file_closed(self, path):
        shownName = 'untitled.txt'
        self.setWindowTitle(self.tr("%s[*] - %s" % (shownName, "Sassy Logic")))
        self.setWindowModified(False)
        model = QtCore.QStringListModel([], parent=self.mappingsListView)
        self.mappingsListView.setModel(model)
        sm = self.mappingsListView.selectionModel()
        sm.currentChanged.connect(self.on_template_selected)
        self.predicateText.setText('')
        self.templateText.setText('')
        
    def on_file_modified(self, data):
        get_log().debug('Wndow modified.')
        self.setWindowModified(True)

    def on_template_added(self, data):
        if data:
            string = format_mapping(data)
            model = self.mappingsListView.model()
            model.insertRow(model.rowCount())
            index = model.index(model.rowCount()-1)
            model.setData(index, string)
            model.sort(0)
        self.updateMappingButton.setEnabled(False)
    
    def on_template_deleted(self, data):
        string = format_mapping(data)
        model = self.mappingsListView.model()
        sl = model.stringList()
        newSl = list(filter(lambda x: x != string, sl))
        model.setStringList(newSl)
        model.sort(0)
        self.deleteMappingButton.setEnabled(False)
        self.updateMappingButton.setEnabled(False)

    def on_template_text_changed(self):
        """ Enable or disable the Add/Delete buttons depending on text. """
        tmp = self.templateText.toPlainText().strip()
        if tmp != '':
            tmp2 = self.predicateText.toPlainText().strip()
            if tmp2 != '':
                self.addMappingButton.setEnabled(True)
                return
        self.addMappingButton.setEnabled(False)

    def on_predicate_text_changed(self):
        pass

    def on_template_selected(self, idx, previous=None):
        """ New template was selected. """
        # enable deletion
        self.deleteMappingButton.setEnabled(True)
        self.addMappingButton.setText('New Template')
        # FIXME: you should implement a custom model that can return the
        #   string when asked for DisplayItemRole (default val. for data())
        #   and the actual pair of entities when called with something like
        #   user role
        data = idx.data() # string
        pair = eval(data) # convert the string to the pair it represents
        self.predicateText.setPlainText(pair[0])
        template = self.doc_ctrl.get_template(pair[0])
        tmp = format_template(template)
        self.templateText.setPlainText(tmp)
        self.updateMappingButton.setEnabled(True)


######################### SimplifiedMainWindow #################################


class SnappyMainWindow(QtWidgets.QMainWindow, Ui_SNAPPY):
    def __init__(self, parent=None, f=QtCore.Qt.WindowFlags()):
        QtWidgets.QMainWindow.__init__(self, parent, f)

        self.setupUi(self)
        # NLG Template file menu
        self.actionNew.triggered.connect(self.handle_new)
        self.actionOpen.triggered.connect(self.handle_open)
#        self.actionClear.triggered.connect(self.handle_clear_recent)
        self.actionSave.triggered.connect(self.handle_save)
        self.actionSave_As.triggered.connect(self.handle_save_as)
        self.actionPrint.triggered.connect(self.handle_print)
        self.actionClose.triggered.connect(self.handle_close)
        self.actionShow_Help.triggered.connect(self.show_help)
        # buttons
        #    NLG Templates
        self.addMappingButton.clicked.connect(self.handle_add_mapping)
        self.updateMappingButton.clicked.connect(self.handle_update_mapping)
        self.deleteMappingButton.clicked.connect(self.handle_delete_mapping)
        self.addMappingButton.setEnabled(False)
        self.deleteMappingButton.setEnabled(False)
        self.updateMappingButton.setEnabled(False)
        #    NLG
        self.translateButton.clicked.connect(self.handle_translate)
        self.simplificationHelpToolButton.clicked.connect(self.show_help)
        # navigation shortcuts
        self.actionNLG.triggered.connect(self.show_nlg)
        self.actionBackground_Theory.triggered.connect(self.show_bgt)
        self.actionNLG_Templates.triggered.connect(self.show_templates)
        # signals
        self.predicateText.textChanged.connect(self.on_predicate_text_changed)
        self.templateText.textChanged.connect(self.on_template_text_changed)
        # Logic controller
        self.logic_ctrl = LogicController()
        # Document controller
        self.doc_ctrl = DocumentController()
        self.doc_ctrl.ask_save_file_path.connect(self.get_save_file_path)
        self.doc_ctrl.ask_open_file_path.connect(self.get_open_file_path)
        self.doc_ctrl.ask.connect(self.ask_question)
        self.doc_ctrl.file_loaded.connect(self.on_file_loaded)
        self.doc_ctrl.file_saved.connect(self.on_file_saved)
        self.doc_ctrl.file_closed.connect(self.on_file_closed)
        self.doc_ctrl.template_added.connect(self.on_template_added)
        self.doc_ctrl.template_deleted.connect(self.on_template_deleted)
        self.doc_ctrl.modified.connect(self.on_file_modified)
        # initialise the default model
        self.on_file_closed(None)
        # NLG controller
        self.nlg_ctrl = NlgController()
        # link it with the doc controller so that templates can be loaded
        self.doc_ctrl.template_deleted.connect(
            self.nlg_ctrl.on_template_deleted)
        self.doc_ctrl.template_added.connect(
            self.nlg_ctrl.on_template_added)
        self.doc_ctrl.file_closed.connect(
            self.nlg_ctrl.on_file_closed)
        self.doc_ctrl.file_loaded.connect(
            self.nlg_ctrl.on_file_loaded)
        self.inputText.setFocus()
        # highlighting for FOL
        FOLHighlighter(self.inputText.document())
        self.inputText.setCompleter(LogicCompleter())
        FOLHighlighter(self.backgroundTheoryText.document())
        self.backgroundTheoryText.setCompleter(LogicCompleter())
        # highlighting for NLG templates
        keywords = ['Element', 'String', 'PlaceHolder', 'Word',
            'Noun', 'Verb', 'Adjective', 'Adverb', 'Preposition', 'Determiner',
            'Pronoun', 'Conjunction', 'Preposition', 'Numeral', 'Symbol',
            'Clause', 'NP', 'VP', 'PP', 'AdvP', 'AdjP',
            'NN', 'NNP', 'NounPhrase', 'VerbPhrase',
            'AdjectivePhrase', 'AdverbPhrase', 'PrepositionalPhrase',
            'Coordination',
            'Features', 'Case', 'Number', 'Gender', 'Person', 'Tense',
            'Aspect', 'Mood', 'Modal', 'Voice', 'Form', 'InterrogativeType'
        ]
        TemplateHighlighter(self.templateText.document(), keywords)
        self.templateText.add_delimiters('(', ')')
        self.templateText.setCompleter(QtWidgets.QCompleter(keywords,
                                           self.templateText))
        self.helpButton.clicked.connect(self.show_help)
#        self.splitter_2.setSizes((200, 0))
        self.parserResponseLabel.setText('')

    def show_help(self):
        """ Create a new label and write the syntax on it. """
        d = HelpDialog(parent=self)
        d.show()
        d.exec_()

    def show_nlg(self):
        get_log().debug('showing NLG tab')
        self.tabWidget.setCurrentIndex(0)
    
    def show_templates(self):
        get_log().debug('showing NLG templates tab')
        self.tabWidget.setCurrentIndex(1)
    
    def show_bgt(self):
        get_log().debug('showing background theory tab')
        self.tabWidget.setCurrentIndex(2)
    
    def handle_new(self):
        get_log().debug('new')
        self.doc_ctrl.new()

    def handle_open(self):
        get_log().debug('open')
        return self.doc_ctrl.open(None) # ask for path
    
    def get_open_file_path(self, question):
        if getattr(sys, 'frozen', False): # frozen
            here = os.path.dirname(sys.executable)
            path = os.path.join(here, '..', '..')
        else: # unfrozen
            here = os.path.dirname(os.path.realpath(__file__))
            path = os.path.join(here, '..', '..')

        fn = QtWidgets.QFileDialog.getOpenFileName(self, "Open File...", path,
                "SassyLogic-Files (*.logic);;All Files (*)")
        if fn and fn[0] != '':
            question.answer = fn[0]
            return True
        return False

    def handle_save(self):
        get_log().debug('save')
        return self.doc_ctrl.save()
        
    def get_save_file_path(self, question):
        fn = QtWidgets.QFileDialog.getSaveFileName(self, "Save as...", None,
            "Sassy Logic files (*.logic);;Text-Files (*.txt);;All Files (*)")

        if not fn or fn[0] == '':
            return False

        question.answer = fn[0]
        return True

    def handle_save_as(self):
        get_log().debug('save as')
        q = Question()
        if self.get_save_file_path(q):
            return self.doc_ctrl.save_as(q.answer)
        return False

    def handle_print(self):
        get_log().debug('print...not implemented')

    def handle_close(self, e):
        get_log().debug('close')
        return self.doc_ctrl.close()
            
    def closeEvent(self, e):
        get_log().debug('close')
        if self.doc_ctrl.close():
            e.accept()
        else:
            e.ignore()


# handles for edit menu

    def handle_add_mapping(self):
        if self.addMappingButton.text() == 'New Template':
            get_log().debug('new mapping')
            # a template was selected before - clear the selection
            self.addMappingButton.setText('Add')
            self.addMappingButton.setEnabled(False)
            self.predicateText.setText('')
            self.templateText.setText('')
            return
        # otherwise, add new mapping
        get_log().debug('add mapping')
        predicate = self.predicateText.toPlainText()
        template = self.templateText.toPlainText()
        try:
            self.doc_ctrl.add_template(predicate, template)
            self.predicateText.setText('')
            self.templateText.setText('')
        except Exception as e:
            get_log().exception('Exception while adding a template.')
            QtWidgets.QMessageBox.warning(self, "Application", str(e), 
                QtWidgets.QMessageBox.Ok)

    def handle_delete_mapping(self):
        """ User wants to delete mapping so remove it from the document. """
        get_log().debug('delete mapping')
        predicate = self.predicateText.toPlainText()
#        if not predicate: return False
        try:
            self.doc_ctrl.del_template(predicate)
            self.predicateText.setText('')
            self.templateText.setText('')
        except Exception as e:
            QtWidgets.QMessageBox.warning(self, "Application", str(e), 
                QtWidgets.QMessageBox.Ok)

    def handle_update_mapping(self):
        """ Update existing template. """
        get_log().debug('update mapping')
        predicate = self.predicateText.toPlainText()
        template = self.templateText.toPlainText()
        get_log().debug('Predicate "{0}" will map to:\n{1}'
                        .format(predicate, template))
        try:
            self.doc_ctrl.add_template(predicate, template, override=True)
            self.predicateText.setText('')
            self.templateText.setText('')
            self.updateMappingButton.setEnabled(False)
        except Exception as e:
            QtWidgets.QMessageBox.warning(self, "Application", str(e), 
                QtWidgets.QMessageBox.Ok)

                
# handle translation

    def handle_translate(self):
        get_log().debug('translate')
        text = self.inputText.toPlainText()
        direct_error = ''
        try:
            f = self.nlg_ctrl.parse_formulas(text)
            self.parserResponseLabel.setText('Formula parsed correctly.')
            self.parserStatusLabel.setPixmap(QtGui.QPixmap(":/images/yes.png"))
            t = self.nlg_ctrl.translate_formulas(f, simple=True)
            f_str = str(f)
            get_log().debug('Formula translated as:\n{0}'.format(f_str))
        except FormulaParseError as e:
            self.parserResponseLabel.setText(str(e))
            self.parserStatusLabel.setPixmap(QtGui.QPixmap(":/images/no.png"))
            t = ''
            f = ''
            f_str = ''
            direct_error += str(e)
        except Exception as e:
            direct_error += str(e)
            t = self.nlg_ctrl.translate_formulas(f, simple=True)
            f_str = str(f)
            get_log().debug('Formula translated as:\n{0}'.format(f_str))

        # show the text to the user
        if direct_error:
            msg = ('<span style="color: red">Exception: {}</span><br><br>{}'
                    .format(error, t))
        else:
            msg = t
        
        self.directTranslationText.setHtml(msg)
        self.directFormulaText.setText(f_str)
        simplified_error = ''
        if not f: return
        # apply simplifications
        try:
            simpl_ops_str = self.simplificationMethodEdit.text()
            axioms = self.get_axioms()
            f2 = self.nlg_ctrl.simplify_formulas(f, simpl_ops_str, axioms)
            t2 = self.nlg_ctrl.translate_formulas(f2, simple=True)
            f2_str = str(f2)
            get_log().debug('Simplified formula translated as:\n{0}'
                            .format(f2_str))
        except Exception as e:
            get_log().exception('Exception while simplifying formula\n{0}'
                                .format(f_str))
            simplified_error += str(e)
            try:
                f2 = self.nlg_ctrl.parse_formulas(text)
                t2 = self.nlg_ctrl.translate_formulas(f, simple=True)
                f2_str = str(f)
            except Exception:
                simplified_error += str(e)

        ## optimised translation
        if simplified_error:
            msg = ('<span style="color: red">Exception: {}</span><br><br>{}'
                    .format(simplified_error, t2))
        else:
            msg = t2
        self.simplifiedTranslationText.setHtml(msg)
        self.simplifiedFormulaText.setText(f2_str)

    def get_axioms(self):
        """ Parse the set of axioms from the model tab and return them as 
        a list of Expr.
        
        """
        axioms = []
        try:
            text = self.backgroundTheoryText.toPlainText()
            axioms = self.nlg_ctrl.parse_formulas(text)
        except FormulaParseError as e:
            self.parserResponseLabel.setText('Error in parsing '
                'Background Theory:\n{0}'.format(e))
            self.parserStatusLabel.setPixmap(QtGui.QPixmap(":/images/no.png"))
        return axioms
    

# handle questions

    def ask_question(self, q):
        buttons = QtWidgets.QMessageBox.Cancel
        if 'save' in q.options:
            buttons |= QtWidgets.QMessageBox.Save
        if 'discard' in q.options:
            buttons |= QtWidgets.QMessageBox.Discard
        if 'yes' in q.options:
            buttons |= QtWidgets.QMessageBox.Yes
        if 'no' in q.options:
            buttons |= QtWidgets.QMessageBox.No
        
        mb = QtWidgets.QMessageBox
        ret = mb.warning(self, "Application", q.text, buttons)

        if ret == QtWidgets.QMessageBox.Save:
            q.answer = 'save'
        if ret == QtWidgets.QMessageBox.Discard:
            q.answer = 'discard'
        if ret == QtWidgets.QMessageBox.Yes:
            q.answer = 'yes'
        if ret == QtWidgets.QMessageBox.No:
            q.answer = 'no'
        if ret == QtWidgets.QMessageBox.Cancel:
            q.answer = 'cancel'

# respond to signals

    def on_file_loaded(self, path):
        shownName = self.doc_ctrl.doc.file_name()
        self.setWindowTitle(self.tr("%s[*] - %s" % (shownName, "Sassy Logic")))
        self.setWindowModified(False)
        strings = doc_to_string(self.doc_ctrl.doc)
        model = QtCore.QStringListModel(strings,
                                        parent=self.mappingsListView)
        model.sort(0)
        self.mappingsListView.setModel(model)
        sm = self.mappingsListView.selectionModel()
        sm.currentChanged.connect(self.on_template_selected)

    def on_file_saved(self, path):
        shownName = self.doc_ctrl.doc.file_name()
        self.setWindowTitle(self.tr("%s[*] - %s" % (shownName, "Sassy Logic")))
        self.setWindowModified(False)
        
    def on_file_closed(self, path):
        shownName = 'untitled.txt'
        self.setWindowTitle(self.tr("%s[*] - %s" % (shownName, "Sassy Logic")))
        self.setWindowModified(False)
        model = QtCore.QStringListModel([], parent=self.mappingsListView)
        self.mappingsListView.setModel(model)
        sm = self.mappingsListView.selectionModel()
        sm.currentChanged.connect(self.on_template_selected)
        self.predicateText.setText('')
        self.templateText.setText('')
        
    def on_file_modified(self, data):
        get_log().debug('Wndow modified.')
        self.setWindowModified(True)

    def on_template_added(self, data):
        if data:
            string = format_mapping(data)
            model = self.mappingsListView.model()
            model.insertRow(model.rowCount())
            index = model.index(model.rowCount()-1)
            model.setData(index, string)
            model.sort(0)
        self.updateMappingButton.setEnabled(False)
    
    def on_template_deleted(self, data):
        string = format_mapping(data)
        model = self.mappingsListView.model()
        sl = model.stringList()
        newSl = list(filter(lambda x: x != string, sl))
        model.setStringList(newSl)
        model.sort(0)
        self.deleteMappingButton.setEnabled(False)
        self.updateMappingButton.setEnabled(False)

    def on_template_text_changed(self):
        """ Enable or disable the Add/Delete buttons depending on text. """
        tmp = self.templateText.toPlainText().strip()
        if tmp != '':
            tmp2 = self.predicateText.toPlainText().strip()
            if tmp2 != '':
                self.addMappingButton.setEnabled(True)
                return
        self.addMappingButton.setEnabled(False)

    def on_predicate_text_changed(self):
        pass

    def on_template_selected(self, idx, previous=None):
        """ New template was selected. """
        # enable deletion
        self.deleteMappingButton.setEnabled(True)
        self.addMappingButton.setText('New Template')
        # FIXME: you should implement a custom model that can return the
        #   string when asked for DisplayItemRole (default val. for data())
        #   and the actual pair of entities when called with something like
        #   user role
        data = idx.data() # string
        pair = eval(data) # convert the string to the pair it represents
        self.predicateText.setPlainText(pair[0])
        template = self.doc_ctrl.get_template(pair[0])
        tmp = format_template(template)
        self.templateText.setPlainText(tmp)
        self.updateMappingButton.setEnabled(True)


############################# TemplateModel ####################################


class TemplateModel(QAbstractListModel):
    """ A model for maintaining the mappings from predicates to text. """

    def __init__(self, document, parent=None):
        """ Construct a model that contains the templates stored in the doc. """
        super(TemplateModel, self).__init__(parent)
        if document is None:
            raise Exception('TemplateModel needs a valid document.')
        self.document = document
        self.keys = []
        for i, k in enumerate(document.keys):
            self.keys[i] = k

    def rowCount(self):
        """ Return the number of items in the model. """
        return len(self.keys)

    def data(self, index, role=Qt.DisplayRole):
        """ Return the data corresponding to an index and the role. """
        if index.isValid() and index.row() < self.rowCount():
            template = self.document(self.keys[index.row()])

            if role == Qt.DisplayRole:
                data = QVariant(format_mapping(template))
            elif role == Qt.SizeHintRole:
                # calculate something based on the filepath
                s = str(format_mapping(template))
                vo = self.viewOptions() # font, colours, ...
                fm = QFontMetrics(vo.font()) # create a font metric from the font
                data = QVariant(fm.width(s)) # find the text width
            else:
                data = QVariant()
        else:
            data = QVariant()

        return data

    def setData(self, index, data, role=Qt.DisplayRole):
        """ Set the data at a given index for the specified role.
        Returns true if successful; otherwise returns false.
        The dataChanged() signal should be emitted if the data
        was successfully set.

        Use Qt.DisplayRole for setting the actual template.
        data -- a pair of ('key', template).
        
        """
        if index.isValid() and index.row() < self.rowCount():
            if role == Qt.DisplayRole:
                self.document.add_template(*data)
                self.dataChanged(index, index)
                return True
        return False

    def insertRows(self, row, count, parent=QModelIndex()):
        """ An insertRows() implementation must call beginInsertRows() before
        inserting new rows into the data structure, and it must call
        endInsertRows() immediately afterwards.
        
        """
        if row == self.rowCount():
            self.beginInsertRows(QModelIndex(), row, row)
            for i in range(count):
                self.keys.append('')
            self.endInsertRows()
        elif row >= 0 and row < self.rowCount:
            self.beginInsertRows(QModelIndex(), row, row)
            for i in range(count):
                self.keys.insert(row, '')
            self.endInsertRows()
        else:
            return False
        return True

    def removeRows(self, row, count, parent=QModelIndex()):
        """ A removeRows() implementation must call beginRemoveRows() before 
        the rows are removed from the data structure, and it must call 
        endRemoveRows() immediately afterwards.
        
        """
        to_remove = []
        if row >= 0 and (row + count) < self.rowCount:
            self.beginRemoveRows(QModelIndex(), row, row)
            to_remove = self.keys[row:row+count]
            self.endRemoveRows()
            for k in to_remove:
                self.document.del_template(k)
        else:
            return False
        return True

# Help
syntax_def ="""\
Syntax Definition:

constant – a lower case letter followed by a combination of letters, \
digits and underscores 
           (e.g., a, b, c, aConstant, const_1)

variable – question mark symbol (optional) followed by a lower case letter \
and a combination of letters, digits and underscores
           (e.g., ?x, ?y, ?z, ?aVariable, ?var_1)

predicate – an upper case letter followed by a combination of letters, \
digits and underscores
           (e.g., P, Q, R, FatherOf, Relation_1)

symbol – any of { ∧, ∨, ¬, =, ≠, \u2190, \u2192, \u2194, (, ), ∀, ∃ }

term – a variable or a constant

atomic formula
    – if t1 and t2 are terms then t1 = t2 is an atomic formula
    – if t1 and t2 are terms then t1 ≠ t2 is an atomic formula
    – if t1 ...tn are terms and P is a predicate then P(t1,...,tn) is \
an atomic formula

formula 
    – if φ is an atomic formula then:
        φ is a formula
       ¬φ is a formula
    – if φ and ψ are atomic formulas then the following are formulas:
    (φ ∧ ψ)
    (φ ∨ ψ)
    (φ \u21D0 ψ)
    (φ \u21D2 ψ)
    (φ \u21D4 ψ)
    – if ?x is a variable then \u2200 ?x: φ is a formula
    – if ?x is a variable then \u2203 ?x: φ is a formula
    
Every symbol can be entered either directly or by typing the symbol's string \
representation e.g. \u2200 can be written as "forall" and ∧ can be written \
as the word "and".

Here are some examples of well formed formulas:
Man(aristotle)
\u2200 ?x: Man(?x) \u2192 Mortal(?x)

Note that the operator ∧ (or) is inclusive. That means that P(x) ∧ Q(x) means \
that the expression is true if either P(x), or if Q(x) is true or if both \
P(x) and Q(x) are true.

"""


############################# utility functions ################################


def format_template(data):
    return repr(data)

def format_mapping(data):
    return str((data[0], ' '.join(format_template(data[1]).split())))
        
def doc_to_string(doc):
    strings = [format_mapping(x) for x in doc.templates]
    return strings


#############################################################################
##
## Copyright (C) 2014 Roman Kutlak, University of Aberdeen.
## All rights reserved.
##
## This file is part of SAsSy Logic program.
##
## You may use this file under the terms of the BSD license as follows:
##
## "Redistribution and use in source and binary forms, with or without
## modification, are permitted provided that the following conditions are
## met:
##   * Redistributions of source code must retain the above copyright
##     notice, this list of conditions and the following disclaimer.
##   * Redistributions in binary form must reproduce the above copyright
##     notice, this list of conditions and the following disclaimer in
##     the documentation and/or other materials provided with the
##     distribution.
##   * Neither the name of University of Aberdeen nor
##     the names of its contributors may be used to endorse or promote
##     products derived from this software without specific prior written
##     permission.
##
## THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
## "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
## LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
## A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
## OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
## SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
## LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
## DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
## THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
## (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
## OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
##
#############################################################################