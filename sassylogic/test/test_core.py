import unittest
import sys
import os

sys.path.append('sassylogic')
sys.path.append('sassynlg')

from sassylogic.core import Document

from nlg.structures import Clause, NounPhrase, VerbPhrase, PrepositionalPhrase, PlaceHolder

class TestDocument(unittest.TestCase):

    def test_ctor(self):
        d = Document()

    def test_add_template(self):
        d = Document()
        self.assertEqual(0, len(d._templates))
        d.add_template('person(?x)',
            Clause(PlaceHolder('?x'), VerbPhrase('is', NounPhrase('person', 'a'))))
        self.assertEqual(1, len(d._templates))
        t = d.get_template('person(?x)')
        self.assertEqual('?x is a person', str(t))

    def test_del_template(self):
        d = Document()
        d.add_template('x', Clause('this', VerbPhrase('is', PlaceHolder('x'))))
        d.add_template('y', Clause('this', VerbPhrase('is', PlaceHolder('y'))))
        self.assertEqual(2, len(d._templates))
        d.del_template('x')
        self.assertEqual(1, len(d._templates))

    def test_persistence(self):
        d = Document('/tmp/document.logic')
        d.add_template('x', Clause('this', VerbPhrase('is', PlaceHolder('x'))))
        d.add_template('y', Clause('this', VerbPhrase('is', PlaceHolder('y'))))
        self.assertEqual(True, d.is_modified())
        d.save()
        self.assertEqual(False, d.is_modified())
        d2 = Document()
        d2.load('/tmp/document.logic')
        self.assertEqual('this is x', str(d2.get_template('x')))
        self.assertEqual('this is y', str(d2.get_template('y')))
        d2.set_file_path('')
        d2.save()
        self.assertEqual(True, os.path.isfile('./untitled.logic'))
        self.assertEqual('untitled.logic', d2.file_name())
        self.assertEqual('.', d2.file_dir())
#        os.remove('./untitled.logic')
#        os.remove('/tmp/document.logic')



# In(food, microwave) ==> In(beer, fridge)
# (In(food, microwave) | In(beer, fridge)) ==> Celebrate(x, y, z)
# At(lift, f1) & Above(f1, f2) & (forall (p, (Down(p) ==> -Boarded(p)))) ==> (At(lift, f2) & -At(lift, f1))
