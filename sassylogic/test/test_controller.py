import unittest
import sys
import os

sys.path.append('sassylogic')
sys.path.append('sassynlg')

from sassylogic.core import Document
from sassylogic.ui.controller import DocumentController

from nlg.structures import Clause, NounPhrase, VerbPhrase, PrepositionalPhrase, PlaceHolder


class TestController(unittest.TestCase):

    def test_ctor(self):
        c = DocumentController()

    def test_add_template(self):
        c = DocumentController()
        c.template_added.connect(self.item_added_test)
        c.modified.connect(self.item_modified_test)
        c.add_template('person(?x)',
                       "Clause(PlaceHolder('?x'), VerbPhrase('is', NounPhrase('person', 'a')))")
        t = c.get_template('person(?x)')
        self.assertEqual('?x is a person', str(t))

    def test_del_template(self):
        c = DocumentController()
        c.template_deleted.connect(self.item_deleted_test)
        c.modified.connect(self.item_modified_test)
        c.add_template('x', "Clause('this', VerbPhrase('is', PlaceHolder('x')))")
        c.add_template('y', "Clause('this', VerbPhrase('is', PlaceHolder('y')))")
        self.assertEqual(2, len(c.doc._templates))
        c.del_template('x')
        self.assertEqual(1, len(c.doc._templates))

    def test_persistence(self):
        c = DocumentController()
        c.set_file_path('/tmp/document.logic')
        c.add_template('x', "Clause('this', VerbPhrase('is', PlaceHolder('x')))")
        c.add_template('y', "Clause('this', VerbPhrase('is', PlaceHolder('y')))")
        c.file_saved.connect(self.inform)
        self.assertEqual(True, c.doc.is_modified())
        c.save()
        self.assertEqual(False, c.doc.is_modified())
        d2 = Document()
        d2.load('/tmp/document.logic')
        self.assertEqual('this is x', str(d2.get_template('x')))
        self.assertEqual('this is y', str(d2.get_template('y')))
        d2.set_file_path('')
        d2.save()
        self.assertEqual(True, os.path.isfile('./untitled.logic'))
        self.assertEqual('untitled.logic', d2.file_name())
        self.assertEqual('.', d2.file_dir())
        os.remove('./untitled.logic')
        os.remove('/tmp/document.logic')

    def inform(self, doc):
        self.assertEqual('/tmp/document.logic', doc.file_path())
        self.assertEqual(True, os.path.isfile(doc.file_path()))

# helpers

    def item_added_test(self, thing):
        self.assertEqual('person(?x)', thing[0])
        self.assertEqual('?x is a person', str(thing[1]))

    def item_deleted_test(self, thing):
        self.assertEqual('x', thing[0])
        self.assertEqual('this is x', str(thing[1]))

    def item_modified_test(self, data):
        self.assertEqual(2, len(data))



